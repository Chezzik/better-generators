Better Generators
=================


# License 

GNU Lesser General Public License v3 (see https://opensource.org/licenses/lgpl-3.0.html)

Copyright 2015, Chezzik

# Description of mod

This is a mod for the pocketmine server.  It will allow the generation of many types of terrain that aren't included by default.

# Usage

To use this mod, you just need the .phar file (in the top level directory).

# More details

For more details, please see the following forum topic:

https://forums.pocketmine.net/threads/new-plugin-better-generators.11630

# Instructions for creating the .phar file

<TBD>
