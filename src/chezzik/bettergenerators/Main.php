<?php
namespace chezzik\bettergenerators;

use pocketmine\plugin\PluginBase;
use pocketmine\level\generator\Generator;


class Main extends PluginBase {
	public function onEnable() {
	Generator::addGenerator(BetterNether::class, BetterNether::NAME);
	Generator::addGenerator(WackyNether::class, WackyNether::NAME);
	Generator::addGenerator(LavaLumps::class, LavaLumps::NAME);
	Generator::addGenerator(BetterNormal::class, BetterNormal::NAME);
	Generator::addGenerator(ChainedPlatforms::class, ChainedPlatforms::NAME);
	}
}
