<?php

namespace chezzik\bettergenerators;

use pocketmine\block\Block;
use pocketmine\block\Stone;
use pocketmine\level\ChunkManager;
use pocketmine\level\generator\GenerationChunkManager;
use pocketmine\level\generator\GenerationManager;
use pocketmine\level\generator\Generator;
use pocketmine\level\generator\noise\Perlin;
use pocketmine\level\generator\noise\Simplex;
use pocketmine\level\Level;
use pocketmine\math\Vector3 as Vector3;
use pocketmine\utils\Random;

class ChainedPlatforms extends Generator{
    const NAME="ChainedPlatforms";

	/** @var ChunkManager */
	private $level;
	/** @var Random */
	private $random;
	private $waterHeight = 32;
	private $bedrockDepth = 5;

	/** @var Simplex */
	private $noiseBase;

	public function __construct(array $options = []){
	}

	public function getName(){
		return self::NAME;
	}

	public function getSettings(){
		return [];
	}

	public function init(ChunkManager $level, Random $random){
		$this->level = $level;
		$this->random = $random;
		$this->random->setSeed($this->level->getSeed());
		$this->noiseBase = new Simplex($this->random, 4, 1 / 4, 1 / 32);
		$this->random->setSeed($this->level->getSeed());
	}

	public function generateChunk($chunkX, $chunkZ){
		$this->random->setSeed(0xdeadbeef ^ ($chunkX << 8) ^ $chunkZ ^ $this->level->getSeed());

		$noise = Generator::getFastNoise3D($this->noiseBase, 16, 128, 16, 4, 8, 4, $chunkX * 16, 0, $chunkZ * 16);

		$chunk = $this->level->getChunk($chunkX, $chunkZ);

		$biomeCache = [];

		for($x = 0; $x < 16; ++$x){
			for($z = 0; $z < 16; ++$z){
				$minSum = 0;
				$maxSum = 0;
				$weightSum = 0;

				$minSum /= $weightSum;
				$maxSum /= $weightSum;

				$smoothHeight = ($maxSum - $minSum) / 2;

				for($y = 0; $y < 128; ++$y){
					if($y === 0) {
                        $chunk->setBlockId($x, $y, $z, Block::BEDROCK);
                        continue;
                    }
                    $noiseValue = $noise[$x][$z][$y];
                    if (($chunkX + $chunkZ) % 2 == 0)
                        $strength = $this->getBlockStrengthAt($x, $y % 16, $z);
                    else
                        $strength = $this->getBlockStrengthAt($z, ($y + 8) % 16, $x);

                    if($strength == 4 && $noiseValue * $strength > 0.2)
                        $chunk->setBlockId($x, $y, $z, Block::STONE_WALL);
                    elseif($noiseValue * $strength > 0.4)
                        $chunk->setBlockId($x, $y, $z, Block::STONE_BRICK);
                    elseif($noiseValue * $strength > 0.2)
                        $chunk->setBlockId($x, $y, $z, Block::STONE);
                    elseif($y <= 3){
						$chunk->setBlockId($x, $y, $z, Block::WATER);
                    }
				}
			}
		}
	}

    public function getBlockStrengthAt($x, $y, $z){
        if ($x > 7 && $z > 7 && $y >= 13)
            return 8;

        if ($z >= 10 && $z <= 13 && $y == $x + 8)
            return 6;

        if ($x >= 10 && $x <= 13 && $y == 7 - $z)
            return 6;

        if (($z == 10 || $z == 13) && ($y == $x + 9))
            return 4;

        if (($x == 10 || $x == 13) && ($y == 8 - $z))
            return 4;

        return 0;
    }

	public function populateChunk($chunkX, $chunkZ){
	}

	public function getSpawn(){
		return new Vector3(127.5, 129, 127.5);
	}

}
