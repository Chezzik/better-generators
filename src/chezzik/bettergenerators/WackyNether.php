<?php

namespace chezzik\bettergenerators;

use pocketmine\block\Block;
use pocketmine\block\Stone;
use pocketmine\level\ChunkManager;
use pocketmine\level\generator\GenerationChunkManager;
use pocketmine\level\generator\GenerationManager;
use pocketmine\level\generator\Generator;
use pocketmine\level\generator\noise\Perlin;
use pocketmine\level\generator\noise\Simplex;
use pocketmine\level\Level;
use pocketmine\math\Vector3 as Vector3;
use pocketmine\utils\Random;

class WackyNether extends Generator{
    const NAME="WackyNether";

	/** @var Populator[] */
	private $populators = [];
	/** @var ChunkManager */
	private $level;
	/** @var Random */
	private $random;
	private $waterHeight = 32;
	private $bedrockDepth = 5;

	/** @var Simplex */
	private $noiseBase1;
	private $noiseBase2;

	public function __construct(array $options = []){
	}

	public function getName(){
		return self::NAME;
	}

	public function getSettings(){
		return [];
	}

	public function init(ChunkManager $level, Random $random){
		$this->level = $level;
		$this->random = $random;
		$this->random->setSeed($this->level->getSeed());
		//$this->noiseBase = new Simplex($this->random, 4, 1 / 4, 1 / 128);
		$this->noiseBase1 = new Simplex($this->random, 4, 1 / 4, 1 / 32);
		$this->noiseBase2 = new Simplex($this->random, 4, 1 / 4, 1 / 32);
		$this->random->setSeed($this->level->getSeed());
	}

    public function getFractionalNoise(&$noise, $x, $y, $z) {
        // This will use x/2, y, z/2 for getting the noise.  If x or z are odd, then interpolation is used.
        $numValues = 0;
        $sum = 0;

        for ($xx = (int) ($x / 2); 2 * $xx <= $x + 1; $xx++) {
            for ($zz = (int) ($z / 2); 2 * $zz <= $z + 1; $zz++) {
                $sum += $noise[$xx][$zz][$y];
                $numValues++;
            }
        }
        return $sum / $numValues;
    }

	public function generateChunk($chunkX, $chunkZ){
		$this->random->setSeed(0xdeadbeef ^ ($chunkX << 8) ^ $chunkZ ^ $this->level->getSeed());
    	$noise1 = Generator::getFastNoise3D($this->noiseBase1, 10, 128, 10, 2, 8, 2, $chunkX * 8, 0, $chunkZ * 8);

		$this->random->setSeed(0xbeefdead ^ ($chunkX << 8) ^ $chunkZ ^ $this->level->getSeed());
    	$noise2 = Generator::getFastNoise3D($this->noiseBase2, 10, 128, 10, 2, 8, 2, $chunkX * 8, 0, $chunkZ * 8);

		$chunk = $this->level->getChunk($chunkX, $chunkZ);

		for($x = 0; $x < 16; ++$x){
			for($z = 0; $z < 16; ++$z){
				$minSum = 0;
				$maxSum = 0;
				$weightSum = 0;

				$minSum /= $weightSum;
				$maxSum /= $weightSum;

				$smoothHeight = ($maxSum - $minSum) / 2;

				for($y = 0; $y < 128; ++$y){
					if($y === 0) {
                        $chunk->setBlockId($x, $y, $z, Block::BEDROCK);
                        continue;
					}
    			    $nx = $this->getFractionalNoise($noise1, $x, $y, $z);
    			    $ny = $this->getFractionalNoise($noise2, $x, $y, $z);

                    if ($nx * $nx + $ny * $ny > 0.09){
                        if(3 * $nx < $ny && -3 * $nx < $ny)
                            $chunk->setBlockId($x, $y, $z, Block::NETHERRACK);
                        elseif(3 * $ny < $nx && -3 * $ny < $nx)
                            $chunk->setBlockId($x, $y, $z, Block::OBSIDIAN);
                        elseif(3 * $ny > $nx && -3 * $ny > $nx)
                            $chunk->setBlockId($x, $y, $z, Block::STONE);
                        elseif($y <= 3)
                            $chunk->setBlockId($x, $y, $z, Block::LAVA);
                    }
                    elseif($y <= 3)
                        $chunk->setBlockId($x, $y, $z, Block::LAVA);

				}
			}
		}
	}

	public function populateChunk($chunkX, $chunkZ){
	}

	public function getSpawn(){
		return new Vector3(127.5, 129, 127.5);
	}

}
