<?php

namespace chezzik\bettergenerators;

use pocketmine\block\Block;
use pocketmine\block\Stone;
use pocketmine\level\ChunkManager;
use pocketmine\level\generator\GenerationChunkManager;
use pocketmine\level\generator\GenerationManager;
use pocketmine\level\generator\Generator;
use pocketmine\level\generator\noise\Perlin;
use pocketmine\level\generator\noise\Simplex;
use pocketmine\level\Level;
use pocketmine\math\Vector3 as Vector3;
use pocketmine\utils\Random;

class LavaLumps extends Generator{
    const NAME="LavaLumps";

	/** @var Populator[] */
	private $populators = [];
	/** @var ChunkManager */
	private $level;
	/** @var Random */
	private $random;
	private $waterHeight = 62;
	private $bedrockDepth = 5;

	/** @var Simplex */
	private $noiseBase;

	public function __construct(array $options = []){
	}

	public function getName(){
		return self::NAME;
	}

	public function getSettings(){
		return [];
	}

	public function init(ChunkManager $level, Random $random){
		$this->level = $level;
		$this->random = $random;
		$this->random->setSeed($this->level->getSeed());
		//$this->noiseBase = new Simplex($this->random, 4, 1 / 4, 1 / 32);
		//$this->random->setSeed($this->level->getSeed());
	}

	public function generateChunk($chunkX, $chunkZ){
		$this->random->setSeed(0xdeadbeef ^ ($chunkX << 8) ^ $chunkZ ^ $this->level->getSeed());

		//$noise = Generator::getFastNoise3D($this->noiseBase, 16, 128, 16, 4, 8, 4, $chunkX * 16, 0, $chunkZ * 16);

		$chunk = $this->level->getChunk($chunkX, $chunkZ);

        $worleyDist = 3;
        $depth=3;
		$distCache = array_fill(0, 16, array_fill(0, 16, array_fill(0,$depth + 1, pow(16*($worleyDist+1), 2))));

        // Look at the closest 29 chunks.  Place an average of 2 dots/chunk.  Calculate Voronai distances for current chunk.
		for($xc = 0 - $worleyDist; $xc <= $worleyDist; $xc++){
            for($zc = 0 - $worleyDist; $zc <= $worleyDist; $zc++){
                if ($xc * $xc + $zc * $zc > $worleyDist * $worleyDist)
                    // save time, only go through chunks that are close.
                    continue;
                $cRand = new Random(0xdeadbeef ^ (($chunkX + $xc) << 8) ^ ($chunkZ+ $zc) ^ $this->level->getSeed());
                for($x = 0; $x < 16; ++$x){
                    for($z = 0; $z < 16; ++$z){
                        if (($cRand->nextSignedInt() & 0x07F) != 0)
                        //if ($x != 0 || $z != 0)
                            continue;
                        for($x2 = 0; $x2 < 16; ++$x2){
                            for($z2 = 0; $z2 < 16; ++$z2){
                                $xdist = $x + 16 * $xc - $x2;
                                $zdist = $z + 16 * $zc - $z2;
                                $voronaiDist = $xdist * $xdist + $zdist * $zdist;
                                $distCache[$x2][$z2][$depth] = $voronaiDist;
                                sort($distCache[$x2][$z2]);
                            }
                        }
                    }
                }
            }
        }

		for($x = 0; $x < 16; ++$x){
			for($z = 0; $z < 16; ++$z){
                $chunk->setBlockId($x, 0, $z, Block::BEDROCK);
                $myheight = max(0, 30 - sqrt($distCache[$x][$z][$depth - 1]));
				for($y = 1; $y <= $myheight; ++$y)
                    $chunk->setBlockId($x, $y, $z, Block::OBSIDIAN);
				for($y = $myheight; $y < 18; ++$y)
                    $chunk->setBlockId($x, $y, $z, Block::LAVA);
			}
		}
	}

	public function populateChunk($chunkX, $chunkZ){
	}

	public function getSpawn(){
		return new Vector3(127.5, 129, 127.5);
	}

}
